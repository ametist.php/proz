<?php

use Illuminate\Database\Seeder;

class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $topics = [
            "Accounting",
            "Advertising / Public Relations",
            "Aerospace / Aviation / Space",
            "Agriculture",
            "Animal Husbandry = Livestock *",
            "Anthropology",
            "Archaeology",
            "Architecture",
            "Art, Arts & Crafts, Painting",
            "Astronomy & Space",
            "Audit = Finance *",
            "Automation & Robotics",
            "Automotive / Cars & Trucks",
            "Banking = Finance *",
            "Biology (-tech,-chem,micro-)",
            "Botany",
            "Building = Construction *",
            "Business/Commerce (general)",
            "Cars = Auto *",
            "Ceramics = Materials *",
            "Certificates, Diplomas, Licenses, CVs",
            "Chemistry; Chem Sci/Eng",
            "Children's Literature = Poetry & Literature *",
            "Cinema, Film, TV, Drama",
            "Clothing = Textiles *",
            "Communications = Telecommunications *",
            "Computers (general)",
            "Computers: Hardware",
            "Computers: Software",
            "Computers: Systems, Networks",
            "Construction / Civil Engineering",
            "Contracts = Law: Contract *",
            "Cooking / Culinary",
            "Cosmetics, Beauty",
            "DVDs = Media *",
            "Dentistry = Medical: Dentistry *",
            "Economics",
            "Education / Pedagogy",
            "Electronics / Elect Eng",
            "Energy / Power Generation",
            "Engineering (general)",
            "Engineering: Aero = Aerospace *",
            "Engineering: Chem = Chemistry *",
            "Engineering: Civil = Construction *",
            "Engineering: Electrical = Electronics *",
            "Engineering: Industrial",
            "Engineering: Mechanical = Mechanics *",
            "Engineering: Nuclear = Nuclear *",
            "Environment & Ecology",
            "Esoteric practices",
            "Fashion = Textiles *",
            "Fiction = Poetry & Literature *",
            "Film & TV = Cinema *",
            "Finance (general)",
            "Financial Markets = Finance *",
            "Fisheries",
            "Folklore",
            "Food & Drink",
            "Forestry / Wood / Timber",
            "Furniture / Household Appliances",
            "Games / Video Games / Gaming / Casino",
            "Gastronomy = Cooking / Culinary *",
            "Gems, Precious Stones, Metals = Mining *",
            "Genealogy",
            "General / Conversation / Greetings / Letters",
            "Genetics",
            "Geography",
            "Geology",
            "Glass = Materials *",
            "Government / Politics",
            "Graphic Arts = Photo/Imaging *",
            "Health Care = Med: HC *",
            "History",
            "Hotels = Tourism *",
            "Human Resources",
            "IT (Information Technology)",
            "Idioms / Maxims / Sayings",
            "Insurance",
            "International Org/Dev/Coop",
            "Internet, e-Commerce",
            "Investment / Securities",
            "Iron & Steel = Metallurgy *",
            "Journalism",
            "Labor = Human Resources *",
            "Land = Real Estate *",
            "Law (general)",
            "Law: Contract(s)",
            "Law: Patents, Trademarks, Copyright",
            "Law: Taxation & Customs",
            "Leisure = Tourism *",
            "Linguistics",
            "Literature = Poetry & Lit *",
            "Livestock / Animal Husbandry",
            "Logistics = Transport *",
            "Machinery & Tools = Mechanical *",
            "Management",
            "Manufacturing",
            "Maritime = Ships *",
            "Marketing / Market Research",
            "Materials (Plastics, Ceramics, etc.)",
            "Mathematics & Statistics",
            "Mechanics / Mech Engineering",
            "Media / Multimedia",
            "Medical (general)",
            "Medical: Cardiology",
            "Medical: Dentistry",
            "Medical: Health Care",
            "Medical: Instruments",
            "Medical: Pharmaceuticals",
            "Metallurgy / Casting",
            "Meteorology",
            "Metrology",
            "Military / Defense",
            "Mining & Minerals / Gems",
            "Multimedia = Media *",
            "Music",
            "Names (personal, company)",
            "Networking = Computers: Networking *",
            "Nuclear Eng/Sci",
            "Nutrition",
            "Oil & Gas = Petroleum *",
            "Other",
            "Paper / Paper Manufacturing",
            "Patents",
            "Patents = Law: Patents *",
            "Pedagogy = Education *",
            "Petroleum Eng/Sci",
            "Pharmaceuticals = Med: Pharma *",
            "Philosophy",
            "Photography/Imaging (& Graphic Arts)",
            "Physics",
            "Plants = Botany *",
            "Plastic = Materials *",
            "Poetry & Literature",
            "Politics = Government *",
            "Power = Energy *",
            "Printing & Publishing",
            "Psychology",
            "Public relations = Advertising *",
            "Real Estate",
            "Religion",
            "Retail",
            "Robotics = Automation & Robotics *",
            "Rubber = Materials *",
            "SAP",
            "Safety",
            "Sailing = Ships *",
            "Science (general)",
            "Shipping = Transport *",
            "Ships, Sailing, Maritime",
            "Slang",
            "Social Science, Sociology, Ethics, etc.",
            "Software = Computers: Software *",
            "Sports / Fitness / Recreation",
            "Statistics = Mathematics *",
            "Surveying",
            "Symbols / Abbreviations / Acronyms = General *",
            "Tax = Law: Tax *",
            "Telecom(munications)",
            "Textiles / Clothing / Fashion",
            "Tourism & Travel",
            "Trains = Transportation *",
            "Transport / Transportation / Shipping",
            "Travel = Tourism *",
            "Veterinary = Livestock *",
            "Video Games = Games *",
            "Video editing/DVDs = Media *",
            "Wine / Oenology / Viticulture",
            "Wood = Materials *",
            "Wood Industry = Forestry *",
            "Zoology"
        ];

        foreach ($topics as $topic) {
            \App\Topic::query()->insert([
                'name' => $topic
            ]);
        }
    }
}
