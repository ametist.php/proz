<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/glossary', 'GlossaryController@index')->name('glossary.index');
Route::post('/glossary', 'GlossaryController@index');

Route::get('/glossary/create', 'GlossaryController@create')->name('glossary.create');
Route::post('/glossary/create', 'GlossaryController@store');

Route::get('/glossary/edit/{id}', 'GlossaryController@edit')->name('glossary.edit');
Route::post('/glossary/edit/{id}', 'GlossaryController@update');

Route::get('/glossary/{id}', 'GlossaryController@show')->name('glossary.show');

