<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        @if( Auth::user() )

            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link active" href="{{route('glossary.index')}}">Glossaries List</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link active" href="{{route('glossary.show', Auth::user()->id)}}">
                        My Glossaries</a>
                </li>
            </ul>
        @endif

    </div>
</nav>
