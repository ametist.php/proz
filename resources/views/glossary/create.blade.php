@extends('layouts.app')

@section('heading')
    Create Glossary
@endsection

@section('content')

    @include( 'glossary.createForm' )

@endsection
