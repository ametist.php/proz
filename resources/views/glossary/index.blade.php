@extends('layouts.app')

@section('heading')
    Glossaries List
@endsection

@section('content')

    <form method="post" class="form-inline mx-auto" style="width: 400px">

        @csrf

        <div class="form-group mb-2">
            <label for="search" class="">Search term</label>
            <input type="text" class="form-control ml-3" id="search" name="search" value="{{ $search }}">
        </div>

        <button type="submit" class="btn btn-primary mb-2 ml-3">Search</button>

    </form>

    @if(count($glossaries) == 0 && $search != '')
        <div class="alert alert-primary" role="alert">
            Can't find any glossary.
        </div>
    @else
        @foreach( $glossaries as $glossary )

            <div class="card mb-3">
                <div class="card-header">
                    {{ $glossary->topic->name }} : "{{ $glossary->language->name }}" Language
                </div>
                <div class="card-body">
                    @foreach( $glossary->terms as $termRel )
                        <div class="row">
                            <div class="col-sm-2">
                                {{ $termRel->term->name }}
                            </div>

                            <div class="col-sm-10">
                                <ul>
                                @foreach( $termRel->term->translations as $translation )
                                    <li>"{{ $translation->language->name }}" -> {{ $translation->translation  }}  </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    @endforeach

                </div>

                <div class="card-footer">
                    <a href="{{ route( 'glossary.edit', $glossary->id ) }}">Add / Edit translations</a>
                </div>
            </div>

        @endforeach
    @endif




@endsection
