@extends('layouts.app')

@section('heading')
    Edit Glossary
@endsection

@section('content')

    @include( 'glossary.editForm' )

@endsection
