<form method="post">

    @csrf

    <div class="form-group">
        <label for="topic">Topic</label>
        <input type="text" value="{{  $glossary->topic->name }}" readonly class="form-control" name="topicName">
        <input type="hidden" value="{{  $glossary->topic->id }}" readonly class="form-control" name="topic">
        @error('topic')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label>Base Language</label>
        <input type="text" value="{{  $glossary->language->name }}" readonly class="form-control" name="languageName">
        <input type="hidden" value="{{  $glossary->language->id }}" readonly class="form-control" name="language">
        @error('language')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

    <h3 class="border-bottom mb-3">Terms</h3>

    <div id="termsContainer">
        @foreach( $terms as $termKey => $term )
            <div class="termTranslation">
                @php $key = $term->id ?? $termKey; @endphp
                <div class="form-group row ">
                    <div class="col-md-4 mb-3">
                        <label for="term{{$key}}" class="col-form-label col-form-label-sm">Term: </label>
                        <input type="text" value="{{ $term->term->name }}" readonly class="form-control form-control-sm" name="term[{{$key}}]" id="term{{$key}}" placeholder="New Term" required>
                    </div>

                    <div id="translations[{{$key}}]" class="translations col-md-8 mb-3">

                        @foreach( $term->term->translations as $translationKey => $translation )

                        <div class="row translateRow">
                            <div class="col-md-6 mb-3">
                                <label class="col-form-label col-form-label-sm">Language: </label>
                                <select class="form-control form-control-sm" data-live-search="true" name="termLanguage[{{$key}}][]" required>
                                    <option selected></option>
                                    @foreach($languages as $language)
                                        <option value="{{$language->id}}" {{ old('language', $translation->language_id) == $language->id ? 'selected' : '' }}>{{$language->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label class="col-form-label col-form-label-sm ">Translation:</label>
                                <input type="text" value="{{ $translation->translation}}" class="form-control form-control-sm" name="termTranslation[{{$key}}][]" required>
                            </div>
                        </div>
                        @endforeach


                        <div class="col text-right">
                            <button class="btn btn-link addTermTranslation text-right" type="button">Add Translation</button>
                        </div>

                    </div>

                </div>

                <hr class="mb-3">
            </div>
        @endforeach
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
