@extends('layouts.app')

@section('heading')
    My Glossaries
@endsection

@section('content')
    <nav class="clearfix mb-3">
        <a href="{{route('glossary.create')}}" class="btn btn-light float-right">Create Glossary</a>
    </nav>

    @if($glossaries->count() == 0)
        <div class="alert alert-primary" role="alert">
            You haven't created any glossary yet.
        </div>
    @else
        @foreach( $glossaries as $glossary )

            <div class="card mb-3">
                <div class="card-header">
                    {{ $glossary->topic->name }} : "{{ $glossary->language->name }}" Language
                </div>
                <div class="card-body">
                    @foreach( $glossary->terms as $termRel )
                        <div class="row">
                            <div class="col-sm-2">
                                {{ $termRel->term->name }}
                            </div>

                            <div class="col-sm-10">
                                <ul>
                                @foreach( $termRel->term->translations as $translation )
                                    <li>"{{ $translation->language->name }}" -> {{ $translation->translation  }}  </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    @endforeach

                </div>

                <div class="card-footer">
                    <a href="{{ route( 'glossary.edit', $glossary->id ) }}">Add / Edit translations</a>
                </div>
            </div>

        @endforeach
    @endif

@endsection
