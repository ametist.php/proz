<form method="post">

    @csrf

    <div class="form-group">
        <label for="topic">Topic</label>
        <select class="custom-select @error('topic') is-invalid @enderror" name="topic" id="topic" required>
            <option selected></option>
            @foreach($topics as $topic)
                <option value="{{$topic->id}}" {{ old('topic') == $topic->id ? 'selected' : '' }} >{{$topic->name}}</option>
            @endforeach
        </select>
        @error('topic')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label>Base Language</label>
        <select class="custom-select  @error('language') is-invalid @enderror" name="language" required>
            <option selected></option>
            @foreach($languages as $language)
                <option value="{{$language->id}}" {{ old('language') == $language->id ? 'selected' : '' }} >{{$language->name}}</option>
            @endforeach
        </select>
        @error('language')
            <div class="text-danger">{{ $message }}</div>
        @enderror
        <small id="languageHelp" class="form-text text-muted">Please, select base language</small>
    </div>

    <h3 class="border-bottom mb-3">Terms</h3>

    <div id="termsContainer">

        @foreach( $terms as $termKey => $term )

            <div class="termTranslation">

                @php $key = $term->id ?? $termKey; @endphp

                <div class="form-group row ">

                    <div class="col-md-4 mb-3">
                        <label for="term{{$key}}" class="col-form-label col-form-label-sm">Term: </label>
                        <input type="text" class="form-control form-control-sm" name="term[{{$key}}]" id="term{{$key}}" placeholder="New Term" required>
                    </div>

                    <div id="translations[{{$key}}]" class="translations col-md-8 mb-3">

                        <div class="row translateRow">

                            <div class="col-md-6 mb-3">
                                <label class="col-form-label col-form-label-sm">Language: </label>
                                <select class="form-control form-control-sm" name="termLanguage[{{$key}}][]" required>
                                    <option selected></option>
                                    @foreach($languages as $language)
                                        <option value="{{$language->id}}">{{$language->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label class="col-form-label col-form-label-sm ">Translation:</label>
                                <input type="text" class="form-control form-control-sm" name="termTranslation[{{$key}}][]" required>
                            </div>

                        </div>

                        <div class="col text-right">
                            <button class="btn btn-link addTermTranslation text-right" type="button">Add Translation</button>
                        </div>

                    </div>

                </div>

                <hr class="mb-3">
            </div>
        @endforeach
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
