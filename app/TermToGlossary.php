<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermToGlossary extends Model
{
    public $timestamps = false;

    protected $fillable = ['term_id', 'glossary_id'];


    public function term()
    {
        return $this->belongsTo(Term::class);
    }

    public function glossary()
    {
        return $this->belongsTo(Glossary::class);
    }


}
