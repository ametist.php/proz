<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TranslationToTerm extends Model
{
    public $timestamps = false;
    protected $fillable = ['translation_id', 'term_id'];
}
