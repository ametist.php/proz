<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $fillable = ['language_id', 'name'];


    public function translations()
    {
        return $this->hasMany(Translation::class);
    }

    public function glossary()
    {
        return  $this->hasOne(TermToGlossary::class);
    }

}
