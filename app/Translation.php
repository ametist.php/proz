<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    public $timestamps = false;

    protected $fillable = ['term_id', 'translation', 'language_id'];

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function term()
    {
        return $this->belongsTo(Term::class);
    }


}
