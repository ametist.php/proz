<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Glossary extends Model
{
    protected $fillable = ['topic_id', 'user_id', 'language_id'];


    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function terms()
    {
        return $this->hasMany(TermToGlossary::class);
    }







    /**
     * @param int $id
     * @return Collection
     */
    public static function userGlossaries(int $id): Collection
    {
        return self::query()->where('user_id', $id)->get();
    }
}
