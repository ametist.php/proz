<?php

namespace App\Http\Controllers;

use App\Glossary;
use App\Language;
use App\Term;
use App\TermToGlossary;
use App\Topic;
use App\Translation;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GlossaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $glossaries = [];

        if ($request->method() == 'POST') {
            $searchStr = $request->post('search');

            $terms = Term::query()->where('name', 'LIKE', '%' . $searchStr . '%')->get();
            $translates = Translation::query()->where('translation', 'LIKE', '%' . $searchStr . '%')->get();
            $glossaries = [];
            foreach ($terms as $term) {
                $glossary = $term->glossary->glossary;
                $glossaries[$glossary->id] = $glossary;
            }
            foreach ($translates as $translate) {
                $glossary = $translate->term->glossary->glossary;
                $glossaries[$glossary->id] = $glossary;
            }
        }

        return view('glossary.index')
            ->with('glossaries', $glossaries)
            ->with('search', $searchStr ?? '');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('glossary.create')
            ->with('topics', Topic::all())
            ->with('languages', Language::all())
            ->with('terms', factory('App\Term', 3)->make());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'topic' => 'required',
            'language' => 'required',
        ]);

        $termLanguages = $request->post('termLanguage');
        $termTranslations = $request->post('termTranslation');
        $languageId = $request->post('language');

        $translations = [];
        foreach ($termLanguages as $termLanguageKey => $termLanguage) {
            $translations[$termLanguageKey] = array_combine($termLanguage, $termTranslations[$termLanguageKey]);
        }

        // Store
        $glossary = Glossary::query()->create([
            'topic_id' => $request->post('topic'),
            'language_id' => $languageId,
            'user_id' => Auth::user()->id
        ]);

        foreach ($request->post('term') as $termKey => $termName) {

            $term = Term::query()->firstOrCreate([
                'language_id' => $languageId,
                'name' => $termName
            ]);

            TermToGlossary::query()->firstOrCreate([
                'term_id' => $term->id,
                'glossary_id' => $glossary->id
            ]);

            $termTranslations = $translations[$termKey];

            foreach ($termTranslations as $language_id => $termTranslation) {
                $translation = Translation::query()->firstOrCreate([
                    'term_id' => $term->id,
                    'language_id' => $language_id,
                    'translation' => $termTranslation
                ]);
            }
        }

        return redirect(route('glossary.show', $glossary->user_id));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $glossaries = Glossary::userGlossaries($id);

        return view('glossary.show', compact('glossaries'));
    }


    public function search(Request $request)
    {


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $glossary = Glossary::query()->find($id);

        return view('glossary.edit')
            ->with('topics', Topic::all())
            ->with('languages', Language::all())
            ->with('glossary', $glossary)
            ->with('terms', $glossary->terms);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $glossary = Glossary::find($id);

        $termLanguages = $request->post('termLanguage');
        $termTranslations = $request->post('termTranslation');
        $languageId = $request->post('language');

        $translations = [];
        foreach ($termLanguages as $termLanguageKey => $termLanguage) {
            $translations[$termLanguageKey] = array_combine($termLanguage, $termTranslations[$termLanguageKey]);
        }

        foreach ($request->post('term') as $termKey => $termName) {

            $term = Term::query()->firstOrCreate([
                'language_id' => $languageId,
                'name' => $termName
            ]);

            TermToGlossary::query()->firstOrCreate([
                'term_id' => $term->id,
                'glossary_id' => $glossary->id
            ]);

            $termTranslations = $translations[$termKey];

            foreach ($termTranslations as $language_id => $termTranslation) {
                $translation = Translation::query()->firstOrCreate([
                    'term_id' => $term->id,
                    'language_id' => $language_id,
                ]);

                $translation->translation = $termTranslation;
                $translation->save();

            }
        }

        return redirect(route('glossary.show', $glossary->user_id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
